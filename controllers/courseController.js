const Course = require('../models/course');

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, err) => {
		if(err) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.getAllCourses = async (data) => {
	if(data) {
		return Course.find({}).then(result => {
			return result;
		});
	} else {
		return false;
	};
};

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err) => {
		if(err) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.archiveCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		isActive: reqBody.isActive
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err) => {
		if(err) {
			return false;
		} else {
			return true;
		};
	});
};
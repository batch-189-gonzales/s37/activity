const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');

router.post("/", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send("Not accessible to user");
    };
});

router.get("/all", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	courseController.getAllCourses(isAdmin).then(resultFromController => res.send(resultFromController));
});

router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:courseId", auth.verify, (req, res) => {const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
    	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send("Not accessible to user");
    };
});

router.put("/:courseId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
    	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send("Not accessible to user");
    };
});

module.exports = router;